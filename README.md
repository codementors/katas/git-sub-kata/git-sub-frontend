# Git frontend with git subtree

Frontend uses the git-sub-api as a subtree

Subtrees are easier to pull but harder to push – This is because they are copies of the original repository

## Add a project as subtree

```
git subtree add --prefix git-sub-api git@gitlab.com:codementors/katas/git-sub-kata/git-sub-api.git main --squash
```

## Pulling changes from remote

```
git subtree pull --prefix git-sub-api git@gitlab.com:codementors/katas/git-sub-kata/git-sub-api.git main --squash
```

## Pushing changed to subtree

```
git subtree push --prefix git-sub-api git@gitlab.com:codementors/katas/git-sub-kata/git-sub-api.git main
```

## Practice

- Try to use a common workflow (feature branch -> rebase master -> merge)